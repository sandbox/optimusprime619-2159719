This module is intended to provide a simple functionality of indexing content
with their corresponding user's name.

The module makes use of the core search API to add the mapping of node's id and
respective author (user) name with a type of nodebyauthor.

Configuration:
==============
1. Enable the "Node search by Author" module in Administer >> Modules.
2. Navigate to Administer >> Configuration >> Search Settings and open Search 
and Meta data.
3. Under "Active search modules" check the Node search by Author. Optionally,
Select the same in "Default Search module" if you wish to have this set as 
the primary search result page.
4. Click Re-index site and select reindex in the confirmation form.
Run cron to reindex content.
5. Search for content and the search results page should contain an additional
option named "Search Content by author".


Though there are more powerful modules for enhanced search modules, this module
should suffice for those running the core search functionality and also could 
serve as an example for understanding core search module's API.
